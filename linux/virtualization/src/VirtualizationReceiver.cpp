/*
 * SPDX-License-Identifier: Apache-2.0
 *
 * SPDX-FileCopyrightText: Huawei Inc.
 * SPDX-FileCopyrightText: Politecnico Di Milano
 */

#include <unordered_map>
#include <vector>
#include <glib.h>
#include <gio/gio.h>
#include <string>

#include "common.h"
#include "VirtualizationReceiver.h"
#include "EddieEndpoint.h"

static const gchar *introspection_XML =
        "<node>"
        "  <interface name='org.eddie.TestInterface'>"
        "    <method name='Connect'>"
        "      <arg type='s' name='response' direction='out'/>"
        "    </method>"
        "    <method name='PerformAction'>"
        "      <arg type='s' name='method' direction='in'/>"
        "      <arg type='a{ss}' name='parameters' direction='in'/>"
        "      <arg type='s' name='payload' direction='in'/>"
        "      <arg type='s' name='response' direction='out'/>"
        "    </method>"
        "  </interface>"
        "</node>";

EddieEndpoint* VirtualizationReceiver::eddie_endpoint;
std::vector<EddieResource*> VirtualizationReceiver::resources;
GDBusNodeInfo *VirtualizationReceiver::introspection_data = g_dbus_node_info_new_for_xml(introspection_XML, nullptr);
const GDBusInterfaceVTable VirtualizationReceiver::interface_vtable = {handle_method_call};

std::unordered_multimap<std::string, Link> resources_by_ip;
std::unordered_map<std::string, Link> resources_by_resource_name;

method_t method_from_string(const std::string &input) {
    if (input == "GET") return GET;
    else if (input == "POST") return POST;
    else if (input == "PUT") return PUT;
    else return DELETE;
}

int mock_ai_executor(const std::string &method, const std::unordered_map<std::string, std::string> &query_parameters,
                     std::string &ip, std::string &port, std::string &res, std::string &query) {
    auto preferred_resource_type = query_parameters.find("preferred-resource-type");

    res = "lamp/brightness";
    auto resource_it = resources_by_resource_name.find(res);

    if (resource_it == resources_by_resource_name.end()) {
        LOG_ERR("lamp/brightness resource not found");
        return -1;
    }

    ip = resource_it->second.host;
    port = resource_it->second.port;
    query = "";
    return 0;
}

class MockEddieResource : public EddieResource {
private:
    char* path;
    char* attributes;
    std::vector<char*> split_path;
    std::vector<char*> split_attributes;

    std::string data;

public:
    MockEddieResource(const std::string &path, const std::string &attributes) {
        this->path = new char[path.length()+1];
        strcpy(this->path, path.c_str());

        this->attributes = new char[attributes.length()+1];
        strcpy(this->attributes, attributes.c_str());

        char *token = strtok(this->path, "/");
        while(token) {
            split_path.push_back(token);
            token = strtok(nullptr, "/");
        }
        split_path.push_back(nullptr);

        token = strtok(this->attributes, "&");
        while(token) {
            split_path.push_back(token);
            token = strtok(nullptr, "&");
        }
        split_attributes.push_back(nullptr);

        data = "uninitialized data";
    }

    ~MockEddieResource() {
        delete[] path;
        delete[] attributes;
    }

    message_t render_get(request_t &request) override {
        message_t response;
        response.status_code = CONTENT;
        response.data = this->data;
        return response;
    }

    message_t render_post(request_t &request) override {
        this->data = std::string(reinterpret_cast<const char *>(request.data), request.data_length);
        message_t response;
        response.status_code = CHANGED;
        return response;
    }

    message_t render_put(request_t &request) override {
        return render_post(request);
    }

	const char * const* get_path() override {
		return &split_path[0];
	}

	const char * const* get_attributes() override {
		return &split_attributes[0];
	}
};

void VirtualizationReceiver::handle_method_call(GDBusConnection *connection, const gchar *sender,
                                                const gchar *object_path, const gchar *interface_name,
                                                const gchar *method_name, GVariant *parameters,
                                                GDBusMethodInvocation *invocation, gpointer user_data) {
    if (g_strcmp0(method_name, "Connect") == 0) {
        LOG_DBG("Received Connect method call");
        g_dbus_method_invocation_return_value(invocation, g_variant_new("(s)", "DONE"));
        return;
    } else if (g_strcmp0(method_name, "PerformAction") == 0) {
        LOG_DBG("Received PerformAction method call");
        gchar *method, *payload;
        GVariantIter *iter;
        std::unordered_map<std::string, std::string> query_parameters;

        g_variant_get(parameters, "(sa{ss}s)", &method, &iter, &payload);

        gchar *key, *value;
        while (g_variant_iter_loop(iter, "{ss}", &key, &value)) {
            query_parameters.insert(std::make_pair(key, value));
        }

        std::string ip, port, resource, query;

        update_resources();

        if (mock_ai_executor(method, query_parameters, ip, port, resource, query)) {
            g_dbus_method_invocation_return_value(invocation, g_variant_new("(s)", "ERROR forging the message"));
            LOG_ERR("ERROR forging the message");
            return;
        }
        request_t request {
                ip.c_str(),
                port.c_str(),
                method_from_string(method),
                resource.c_str(),
                nullptr,
                reinterpret_cast<const uint8_t *>(payload),
                strlen(payload),
                true,
                0
        };
        message_t response = eddie_endpoint->get_client()->send_message_and_wait_response(request);

        g_dbus_method_invocation_return_value(invocation, g_variant_new("(s)", response.data.c_str()));
    }
}

void VirtualizationReceiver::on_bus_acquired(GDBusConnection *connection, const gchar *name, gpointer user_data) {
    guint registration_id;

    registration_id = g_dbus_connection_register_object(connection,
                                                        EDDIE_OBJECT,
                                                        introspection_data->interfaces[0],
                                                        &interface_vtable,
                                                        nullptr, nullptr, nullptr);

    g_assert(registration_id > 0);
    printf("Started virtualization layer");
}

void VirtualizationReceiver::on_name_acquired(GDBusConnection *connection, const gchar *name, gpointer user_data) {
    LOG_DBG("[Virtualization Layer]: dbus name acquired -> %s", name);
}

void VirtualizationReceiver::on_name_lost(GDBusConnection *connection, const gchar *name, gpointer user_data) {
    LOG_ERR("[Virtualization Layer]: dbus name lost <- %s", name);
    exit(-1);
}

VirtualizationReceiver::VirtualizationReceiver(const std::string& ip, const std::string& port) {
    eddie_endpoint = new EddieEndpoint(port, ip);
    if (eddie_endpoint->discover_rd())
        eddie_endpoint->init_resource_dir_local();
}

void VirtualizationReceiver::update_resources() {
    auto discover_result = eddie_endpoint->get_resources_from_rd();

    resources_by_ip.erase(resources_by_ip.begin(), resources_by_ip.end());
    resources_by_resource_name.erase(resources_by_resource_name.begin(), resources_by_resource_name.end());

    for (const auto &s_: discover_result) {
        LOG_DBG("discovered resource: ip=%s res_name=%s", s_.host.c_str(), s_.path.c_str());

        resources_by_ip.emplace(s_.host, s_);
        resources_by_resource_name.emplace(s_.path, s_);
    }
}

EddieEndpoint *VirtualizationReceiver::communication_layer() {
    return eddie_endpoint;
}

int VirtualizationReceiver::run(std::vector<std::string> uris, std::vector<std::string> attributes) {
    eddie_endpoint->start_server();

    for (int i = 0; i < uris.size(); ++i) {
        EddieResource* new_resource = new MockEddieResource(uris[i], attributes[i]);
        VirtualizationReceiver::resources.push_back(new_resource);
        VirtualizationReceiver::eddie_endpoint->add_resource(new_resource);
        VirtualizationReceiver::eddie_endpoint->publish_resources();
    }

    guint owner_id;
    GMainLoop *loop;

    owner_id = g_bus_own_name(G_BUS_TYPE_SESSION,
                              EDDIE_DBUS_NAME,
                              G_BUS_NAME_OWNER_FLAGS_NONE,
                              on_bus_acquired,
                              on_name_acquired,
                              on_name_lost,
                              nullptr, nullptr);

    loop = g_main_loop_new(nullptr, FALSE);

    g_main_loop_run(loop);

    g_bus_unown_name(owner_id);
    g_dbus_node_info_unref(introspection_data);

    return 0;
}