/*
 * SPDX-License-Identifier: Apache-2.0
 *
 * SPDX-FileCopyrightText: Huawei Inc.
 * SPDX-FileCopyrightText: Politecnico Di Milano
 */

#include <vector>
#include <string>
#include "VirtualizationReceiver.h"
#include "argparse.hpp"

using namespace std;

void mock_comm_setup(std::vector<std::string> &uris, std::vector<std::string> &attributes) {
    uris = {
            "lamp",
            "lamp/brightness",
            "lamp/color_rgb"
    };

    attributes = {
            "lt=90000&rt=eddie.lamp&ct=40",
            "lt=90000&rt=eddie.lamp.brightness&range=0-1&ct=0",
            "lt=90000&rt=eddie.lamp.color&ct=0"
    };
}

int main(int argc, char *argv[]) {
    argparse::ArgumentParser program("eddie-virt-server");

    program.add_argument("--ip", "-a")
            .help("Ip address")
            .default_value(std::string{"auto"});

    program.add_argument("--port", "-p")
            .help("Port number")
            .scan<'i', int>()
            .default_value(5683);

    program.add_argument("--exampleres", "-e")
            .help("Publish example lamp resources")
            .default_value(false)
            .implicit_value(true);

    try {
        program.parse_args(argc, argv);
    }
    catch (const std::runtime_error& err) {
        std::cerr << err.what() << std::endl;
        std::cerr << program;
        std::exit(1);
    }

    auto port_number = program.get<int>("port");
    auto ip = program.get<std::string>("ip");
    if (ip == "auto") ip = "";

    VirtualizationReceiver receiver = VirtualizationReceiver(ip, std::to_string(port_number));

    vector<string> uris;
    vector<string> attributes;
    if (program["--exampleres"] == true) {
        mock_comm_setup(uris, attributes);
    }
    receiver.run(uris, attributes);
}
